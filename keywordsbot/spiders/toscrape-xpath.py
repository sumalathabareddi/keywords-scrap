# -*- coding: utf-8 -*-
import scrapy


class ToScrapeSpiderXPath(scrapy.Spider):
    name = 'toscrape-xpath'
    start_urls = [
        'https://www.tokopedia.com/p/elektronik/audio/amplifier',
    ]

    def parse(self, response):
        # keywords = response.xpath("//meta[@name='keywords']/@content").extract()
        # yield {
        #     'keywords': keywords
        # }
        for product in response.xpath('//div[@class="css-11s9vse"]'):
            yield {
                'title': product.xpath('./span[@class="css-1bjwylw"]/text()').extract_first(),
                'price': product.xpath('.//div[@class="css-1beg0o7"]/span[@class="css-o5uqvq"]/text()').extract_first(),
                'tags': product.xpath('.//div[@class="css-tpww51"]//div[@class="css-vbihp9"]/span[@class="css-1kr22w3"]/text()').extract(),
                # 'keywords':product.xpath('.//meta[@class="keywords"]/@content').extract() to extract the keywords
            }
        next_page_url = response.xpath('//button[@class="css-13ld6kr-unf-pagination-item e19tp72t1"]/text()').extract_first()
        if next_page_url is not None:
            yield scrapy.Request(response.urljoin["?page=",next_page_url])

