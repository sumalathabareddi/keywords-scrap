# QuotesBot
This is a Scrapy project to scrape keywords from famous people from https://www.tokopedia.com/p/elektronik/audio/amplifier.


## Extracted data

This project extracts quotes, combined with the respective author names and tags.
The extracted data looks like this sample:

    {
        'title': 'HiFi Mini Stereo Car Amplifier Treble Bass Booster',
        'price': 'Rp88.900',
        'tags': ['Jakarta Barat','elektronikmars']
    }


## Spiders

This project contains two spiders and you can list them using the `list`
command:

    $ scrapy list
    toscrape-xpath

spider extract the data from the website, `toscrape-xpath` employs XPath expressions.


## Running the spiders

You can run a spider using the `scrapy crawl` command, such as:

    $ scrapy crawl toscrape-xpath

If you want to save the scraped data to a file, you can pass the `-o` option:
    
    $ scrapy crawl toscrape-xpath -o keywords.json
